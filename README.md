# README #

Lets say if exception occurs in java code or explicitly we would like to throw exception when
some unexpected behaviour we faced, so after exception if we would like to take some decisions 
based on some exception then my poc would work here.


### Project Technical ###
 we are making two Pojo jaxb object one of them  which we are setting with relevant data 
 at the time of exception being thrown from flow or java class, so here we also created a
 custom exceptions with that Pojo as constructor argument 
 so that when we will throw custom exception we can send that fully data loaded pojo.
     so The Purpuse of pojo( jaxb ) to set with Custom Exception class is:
        There is ExceptionClassHolder class where we are getting 
        that what type of custom exception is and the exception meta data and 
        (Sting xml Jaxb object ) we then unmarshall data and then based on that we can manipulate 
         and take business decisons and set to another Pojo ( Jaxb object ) 
         and send back to browser.
     so impotance is we can take any business decision based on that event    
 
### Project improvement ###
1. Instead of java class ExceptionClassHolder we can use the choice exception 
strategy and make those checks.
2.Others optimizations.

### Developer Details ###

* Name : Sajal Biswas
* Mail: sajalbiswas30@gmail.com
* Phone : (+91)9903076025
* Country: India
* Linkedin Profile: https://www.linkedin.com/in/sajal-biswas-a4ab5557/