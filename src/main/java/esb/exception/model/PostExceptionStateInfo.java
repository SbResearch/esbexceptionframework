package esb.exception.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PostExceptionStateInfo {

	String message;
	String reason;
	String code;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}
