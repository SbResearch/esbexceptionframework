package esb.businessProcessor;

import java.io.StringReader;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.xml.sax.InputSource;

import esb.custom.exception.BusinessProcessException;
import esb.exception.model.PostExceptionStateInfo;
import esb.exception.model.PreExceptionStateInfo;

public class BusinessProcessorComponent implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		// TODO Auto-generated method stub
		PreExceptionStateInfo preExceptionStateInfo=new PreExceptionStateInfo();
		Object object=eventContext.getMessage().getPayload();
		String xml=object.toString();
		 String sucessXpath = "record/throwjavacodeexception/text()";
		 String value=getXpathValue(sucessXpath, xml);
		 if(value.equalsIgnoreCase("yes")){
			 preExceptionStateInfo.setCode("1012");	
			 throw new BusinessProcessException(preExceptionStateInfo);
			
		 }
		return object;
	}
	public String getXpathValue(String xpath,String xml) throws XPathExpressionException{
		  String value="";
		  XPath xPath = XPathFactory.newInstance().newXPath();
		  value=xPath.evaluate(xpath, new InputSource(new StringReader(xml)));
		  return value;
	}

}
