package esb.custom.exception;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.mule.api.ExceptionPayload;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import org.mule.util.ExceptionUtils;

import esb.exception.model.PostExceptionStateInfo;
import esb.exception.model.PreExceptionStateInfo;



public class ExceptionClassHolder implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		// TODO Auto-generated method stub

		MuleMessage msg = eventContext.getMessage();
		final ExceptionPayload exceptionPayload = msg.getExceptionPayload();
		
		Throwable exception = exceptionPayload != null ? exceptionPayload.getException() : null;
	    String exceptionData=ExceptionUtils.getRootCause(exception).getMessage();
	     System.out.println("exceptionData inside exception holder : "+exceptionData);
		
		return processException(msg, exception,exceptionData);
	}
	
	private Object processException(MuleMessage msg, Throwable exception,String exceptionData) {
		PostExceptionStateInfo postExceptionStateInfo=new PostExceptionStateInfo();
		postExceptionStateInfo.setMessage("Default exception");
		PreExceptionStateInfo preExceptionStateInfo=new PreExceptionStateInfo();
		String errorMessage = "";
		
		/*different   Exception basis  different  strategy */
		
		if (ExceptionUtils.containsType(exception, esb.custom.exception.CodeValidationException.class)) {
		System.out.println("handle the code validation exception");
		 preExceptionStateInfo=unmarshalExceptionPayload(exceptionData);
		 /*business decision we made to either what data we show or next what event we would like to proceed*/
			if(preExceptionStateInfo.getCode().equalsIgnoreCase("1011")){
				postExceptionStateInfo.setCode("1023#");
				postExceptionStateInfo.setMessage("Purchase code need to be authentic");
				postExceptionStateInfo.setReason("Purchase code got expire");
			}
		}
		else if (ExceptionUtils.containsType(exception, esb.custom.exception.BusinessProcessException.class)) {
			System.out.println("handle the business  exception");
			 preExceptionStateInfo=unmarshalExceptionPayload(exceptionData);
			 /*business decision we made to either what data we show or next what event we would like to proceed*/
				if(preExceptionStateInfo.getCode().equalsIgnoreCase("1012")){
					postExceptionStateInfo.setCode("1024#");
					postExceptionStateInfo.setMessage("transaction date need to check");
					postExceptionStateInfo.setReason("transaction date is greater than expiry date");
				}
		}
		return postExceptionStateInfo;
	}
	
	public  PreExceptionStateInfo unmarshalExceptionPayload(String xml) {
		  PreExceptionStateInfo preExceptionStateInfo =new PreExceptionStateInfo();
		try{
	    JAXBContext jaxbContext = JAXBContext.newInstance(PreExceptionStateInfo.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StringReader reader = new StringReader(xml);
	    preExceptionStateInfo = (PreExceptionStateInfo) jaxbUnmarshaller.unmarshal(reader);
		}catch(JAXBException jaxbe){
			
		}
	    return preExceptionStateInfo;
	  }
}
