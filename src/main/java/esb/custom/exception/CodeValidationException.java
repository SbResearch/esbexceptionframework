package esb.custom.exception;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import esb.exception.model.PreExceptionStateInfo;

public class CodeValidationException extends RuntimeException{
	PreExceptionStateInfo preExceptionStateInfo;
	public CodeValidationException(PreExceptionStateInfo preExceptionStateInfo) {
		super(jaxbObjectToXML(preExceptionStateInfo));
		this.preExceptionStateInfo=preExceptionStateInfo;
		// TODO Auto-generated constructor stub
	}
	public static   String jaxbObjectToXML(PreExceptionStateInfo data){

		String xmlString = "";
		JAXBContext context = null;
		try {
			context = JAXBContext.newInstance(PreExceptionStateInfo.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML
			StringWriter sw = new StringWriter();
			m.marshal(data, sw);
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "");
			xmlString = sw.toString();
					

		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return  xmlString;
	}
	
}
